# Project Foundation

## Introduction

The other spark projects can start from this foundation.

Currently this project uses [TOX](https://tox.readthedocs.io/en/latest/) to manage the following tasks:

- pylint
- pycodestyle
- generate dependencies.

### Requirements:
All source files are in python and bash.

- Python 3.7
- Docker
- Docker Compose

Python3.7 is necessary, because is the same in aws environment.

## Running the application locally

Build the images by docker-compose:

```sh
$ docker-compose build
```

Up containers:

```
$ docker-compose up -d
```

Then, run the image to verify code health:

```sh
$ docker-compose exec app tox
```
And to run application:

```sh
$ docker-compose exec app ./run_spark_submit.sh
```

## Generate dist

To generate distribution with all necessary to deploy is needed run:

```sh
$ docker-compose exec app ./generate_dist.sh
```

Then upload the dist folder to bucket s3 target.

## DATALAKE

The datalake tree must be:

```
DATALAKE
├── RAW
├── SHORE_MART
└── TRUSTED
    ├── CANONICAL_RAW
    └── REFINED
```
