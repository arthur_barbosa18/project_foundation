#!/usr/bin/env bash

#set -o errexit   # make your script exit when a command fails
#set -o pipefail  # exit status of the last command that threw a non-zero exit code is returned
#set -o nounset   # exit when your script tries to use undeclared variables

rm -r dist
mkdir -p dist
pip3 install tox
tox --recreate
cd .tox/py3.7/lib/python3.7/site-packages && zip -r site-packages.zip ./* && mv site-packages.zip ../../../../../dist && cd - || exit
zip -r brasileirao.zip brasileirao/
mv brasileirao.zip dist/
cp main.py dist/