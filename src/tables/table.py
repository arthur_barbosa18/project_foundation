""" Table Module """

from abc import ABC, abstractmethod

from pyspark.sql.session import SparkSession


class Table(ABC):
    """Table is a abstract class.
    It's responsible to load dataset and create
    dataset object
    """

    def __init__(self, spark: SparkSession, path: str, table_name: str):
        self.spark = spark
        self.table_name = table_name
        self.path = path
        self.schema = None
        self.dataframe = None

    @abstractmethod
    def load(self):
        """Load the dataset from path with its schema
        """
    @abstractmethod
    def set_schema(self):
        """Sets the talbe schema
        """

    def save_dataframe(self, path: str, mode="overwrite", _format="parquet"):
        """Save Dataframe
        Args:
            path (str): Path where dataframe will be saved
            mode (str, optional): Save mode. Defaults to "overwrite".
            _format (str, optional): Save format. Defaults to "parquet".
        """
        self.dataframe.write.format(_format).mode(mode).save(path)
