"""This module is reponsible for storing error codes constants """
from jsonschema import validate

ERROR_SCHEMA = {
    "required": ["error_code", "message"],
    "type": "object",
    "properties": {
        "error_code": {"type": "string"},
        "message": {"type": "string"}
    }
}


def get_error(error):
    """ Get error to log messages """
    validate(error, ERROR_SCHEMA)
    error_msg = ""
    for key, value in error.items():
        error_msg = error_msg + key + ": " + value + "\t"
    return error_msg
