""" Aggregates all functions in common use"""
import json
import boto3
from pyspark.sql.types import IntegerType, StringType, StructField, StructType
from src.repo.constants import Constants


def get_hrf_from_s3(path_to_schema: str) -> dict:
    """Get a dict from humand readble file (json) from bucket s3

    Args:
        path_to_schema (str): Path to hrf file in bucket s3

    Returns:
        dict: hrf file converted to dict
    """
    s3_ = boto3.client(Constants.S3)
    result = s3_.get_object(Bucket=Constants.BUCKET, Key=path_to_schema)[
        'Body'].read().decode("UTF-8")
    return json.loads(result)


def convert_hrf_to_schema(hrf: dict) -> StructType:
    """Convert human read json file to spark readable schema

    Args:
        hrf (dict): human readable file

    Returns:
        [StructType]: StructType schema
    """
    struct_field_list = []
    for field_name, field_value in hrf['properties'].items():
        struct_field_list.append(
            StructField(field_name,
                        convert_json_to_spark_type(field_value['type']),
                        nullable=not(field_value['default'])))
    schema = StructType(fields=struct_field_list)
    return schema


def convert_json_to_spark_type(type_: str):
    """Convert attribut type in json file to acceptable type in spark

    Args:
        type_ (str): attribut type in json file

    Returns:
        [Spark Type]: spark type according input
    """
    if type_ == "string":
        return StringType()
    if type_ == "integer":
        return IntegerType()
    return StringType()
