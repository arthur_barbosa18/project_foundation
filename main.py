""" Main Module """

import sys
from pyspark.sql import SparkSession
from awsglue.utils import getResolvedOptions
from src.repo import log
from src.repo.constants import Constants
from src.usecase.matches.handle_trusted import trusted_usecase

def main():
    """ main function"""
    args = getResolvedOptions(sys.argv, ["pipeline", "JOB_NAME", "path_to_datalake"])
    log.info(args)
    spark = SparkSession.builder.appName("brasileirao").getOrCreate()
    Constants.PATH_TO_DATALAKE = args["path_to_datalake"]
   
if __name__ == "__main__":
    main()